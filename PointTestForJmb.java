package Jmb3rdPoint;

import java.io.Serializable;
import java.util.HashMap;
import java.util.TreeMap;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.alibaba.fastjson.JSON;


public class PointTestForJmb {
	
	/**连接超时时间,单位ms*/
	public static final int CONNECTION_TIMEOUT = 1000*30;
	
	/**Socket超时时间,单位ms*/
	public static final int SOCKET_TIMEOUT = 1000*30;
	
	/**测试环境请求地址*/
	private static final String testUrl = "http://test.iquickgo.com:6060";
	
	/**正式环境请求地址*/
	private static final String productUrl = "http://hub.enjar.com";
	
	public static void main(String[] args) {
		//testRegisterCustomer();
		//testObtainCustomer();
		//testObtainMaxPointToPay();
		//testTradeAward();
		//testTradeReverse();
		//testObtainPointExchangeRate();
		//testPointExchange();
		//testPointExchangeReverse();
	}
	
	
	/**
	 * 会员注册
	 * （公司如有设定注册奖励积分会得积分）
	 */
	public static void testRegisterCustomer() {
		try {
			//以下2参数必填
			String com_no = "1025";
			String cellphone = "18659121212";
			
			//以下2参数选填
			String cust_name = "wiley";
			String cust_gender = "M";
			
			String url = testUrl+"/loyalty/api/thirdParty/registerCustomer.json";
			
			
			TreeMap<String, Object> mapReq = new TreeMap<String, Object>();
			mapReq.put("com_no", com_no);
			mapReq.put("cellphone", cellphone);
			mapReq.put("cust_name", cust_name);
			mapReq.put("cust_gender", cust_gender);
			
			String req = buildReq("CustomerRegisterInput", mapReq);
			System.out.println(JSON.toJSONString(req, true));
			System.out.println("================");
			String resp = connRemoteWithJson(req, url);
			System.out.println(JSON.toJSONString(resp, true));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * 获取会员信息
	 */
	public static void testObtainCustomer() {
		try {
			//以下参数必填
			String com_no = "1025";
			String cellphone = "18659121212";
			
			String url = testUrl+"/loyalty/api/thirdParty/obtainCustomer.json";
			
			
			TreeMap<String, Object> mapReq = new TreeMap<String, Object>();
			mapReq.put("com_no", com_no);
			mapReq.put("cellphone", cellphone);
			
			String req = buildReq("CustomerInput", mapReq);
			System.out.println(JSON.toJSONString(req, true));
			System.out.println("================");
			String resp = connRemoteWithJson(req, url);
			System.out.println(JSON.toJSONString(resp, true));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	
	/**
	 *获取单笔订单可使用的积分
	 */
	public static void testObtainMaxPointToPay() {
		try {
			//以下参数必填
			String com_no = "1025";
			String cellphone = "18659121212";
			String total_amount = "100.5";
			
			String url = testUrl+"/loyalty/api/thirdParty/obtainMaxPointToPay.json";
			
			
			TreeMap<String, Object> mapReq = new TreeMap<String, Object>();
			mapReq.put("com_no", com_no);
			mapReq.put("cellphone", cellphone);
			mapReq.put("total_amount", total_amount);
			
			String req = buildReq("PointUsablePayInput", mapReq);
			System.out.println(JSON.toJSONString(req, true));
			System.out.println("================");
			String resp = connRemoteWithJson(req, url);
			System.out.println(JSON.toJSONString(resp, true));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * 消费得积分（可包含积分支付）
	 */
	public static void testTradeAward() {
		try {
			//以下参数必填
			String com_no = "1025";
			String cellphone = "18659212980";
			String total_amount = "100.5";
			long out_trade_no = System.currentTimeMillis();
			boolean point_flag = true;
			//String out_trade_no = "1514276901238";
			
			
			String url = testUrl+"/loyalty/api/thirdParty/tradeAward.json";
			
			
			TreeMap<String, Object> mapReq = new TreeMap<String, Object>();
			mapReq.put("com_no", com_no);
			mapReq.put("cellphone", cellphone);
			mapReq.put("total_amount", total_amount);
			mapReq.put("out_trade_no", out_trade_no);
			mapReq.put("point_flag", point_flag);
			
			String req = buildReq("CalcTradeAwardInput", mapReq);
			System.out.println(JSON.toJSONString(req, true));
			System.out.println("================");
			String resp = connRemoteWithJson(req, url);
			System.out.println(JSON.toJSONString(resp, true));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * 消费撤销
	 */
	public static void testTradeReverse() {
		try {
			//以下参数必填
			String com_no = "1025";
			long out_trade_no = 1517818972950l;
			
			
			String url = testUrl+"/loyalty/api/thirdParty/tradeReverse.json";
			
			
			TreeMap<String, Object> mapReq = new TreeMap<String, Object>();
			mapReq.put("com_no", com_no);
			mapReq.put("out_trade_no", out_trade_no);
			
			String req = buildReq("TradeReverseInput", mapReq);
			System.out.println(JSON.toJSONString(req, true));
			System.out.println("================");
			String resp = connRemoteWithJson(req, url);
			System.out.println(JSON.toJSONString(resp, true));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * 获取兑换积分时增减比率
	 */
	public static void testObtainPointExchangeRate() {
		try {
			//以下参数必填
			String com_no = "1025";
			
			String url = testUrl+"/loyalty/api/thirdParty/obtainPointExchangeRate.json";
			
			
			TreeMap<String, Object> mapReq = new TreeMap<String, Object>();
			mapReq.put("com_no", com_no);
			
			String req = buildReq("PointExchangeRateInput", mapReq);
			System.out.println(JSON.toJSONString(req, true));
			System.out.println("================");
			String resp = connRemoteWithJson(req, url);
			System.out.println(JSON.toJSONString(resp, true));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	
	/**
	 * 积分增减兑换
	 */
	public static void testPointExchange() {
		try {
			//以下参数必填
			String com_no = "1025";
			String cellphone = "18659212980";
			Integer point = 20;
			long out_trade_no = System.currentTimeMillis();
			Integer operation_type = 1;
			Integer rate = 1;
			
			String url = testUrl+"/loyalty/api/thirdParty/pointExchange.json";
			
			
			TreeMap<String, Object> mapReq = new TreeMap<String, Object>();
			mapReq.put("com_no", com_no);
			mapReq.put("cellphone", cellphone);
			mapReq.put("point", point);
			mapReq.put("out_trade_no", out_trade_no);
			mapReq.put("operation_type", operation_type);
			mapReq.put("rate", rate);
			
			String req = buildReq("PointExchangeInput", mapReq);
			System.out.println(JSON.toJSONString(req, true));
			System.out.println("================");
			String resp = connRemoteWithJson(req, url);
			System.out.println(JSON.toJSONString(resp, true));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * 积分增减兑换撤销
	 */
	public static void testPointExchangeReverse() {
		try {
			//以下参数必填
			String com_no = "1025";
			long out_trade_no = 1517817736948l;
			
			String url = testUrl+"/loyalty/api/thirdParty/pointExchangeReverse.json";
			
			
			TreeMap<String, Object> mapReq = new TreeMap<String, Object>();
			mapReq.put("com_no", com_no);
			mapReq.put("out_trade_no", out_trade_no);
			
			String req = buildReq("PointExchangeReverseInput", mapReq);
			System.out.println(JSON.toJSONString(req, true));
			System.out.println("================");
			String resp = connRemoteWithJson(req, url);
			System.out.println(JSON.toJSONString(resp, true));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public static String buildReq(String key,TreeMap<String,Object> mapReq){
		InputRequest input = new InputRequest();
		input.addReqParam(mapReq);
		HashMap<String,InputRequest> mapInput = new HashMap<String,InputRequest>();
		mapInput.put(key, input);
		String req = JSON.toJSONString(mapInput);
		return req;
	}

	static class InputRequest{

		private  InputHeader header = new InputHeader();
		
		private  TreeMap<String,Object> requestData = new TreeMap<String,Object>();

		public void addReqParam(TreeMap<String,Object> map){
			requestData.putAll(map);
		}

		/**
		 * @return the header
		 */
		public InputHeader getHeader() {
			return header;
		}

		/**
		 * @return the requestData
		 */
		public TreeMap<String, Object> getRequestData() {
			return requestData;
		}

		/**
		 * @param header the header to set
		 */
		public void setHeader(InputHeader header) {
			this.header = header;
		}

		/**
		 * @param requestData the requestData to set
		 */
		public void setRequestData(TreeMap<String, Object> requestData) {
			this.requestData = requestData;
		}
		
	}
	
	static class InputHeader implements Serializable {
		
		/**long InputHeader.java**/
		private static final long serialVersionUID = 1L;
		private String version;
		private String transactionReference=""; 
		private String[] clientReferenceId = {"JMB"};
		private String locale;
		
		
		/**
		 * @return the version
		 */
		public String getVersion() {
			return version;
		}


		/**
		 * @return the transactionReference
		 */
		public String getTransactionReference() {
			return transactionReference;
		}


		/**
		 * @return the clientReferenceId
		 */
		public String[] getClientReferenceId() {
			return clientReferenceId;
		}


		/**
		 * @return the locale
		 */
		public String getLocale() {
			return locale;
		}


		/**
		 * @param version the version to set
		 */
		public void setVersion(String version) {
			this.version = version;
		}


		/**
		 * @param transactionReference the transactionReference to set
		 */
		public void setTransactionReference(String transactionReference) {
			this.transactionReference = transactionReference;
		}


		/**
		 * @param clientReferenceId the clientReferenceId to set
		 */
		public void setClientReferenceId(String[] clientReferenceId) {
			this.clientReferenceId = clientReferenceId;
		}


		/**
		 * @param locale the locale to set
		 */
		public void setLocale(String locale) {
			this.locale = locale;
		}


		@Override
		public String toString() {
			return ToStringBuilder.reflectionToString(this);
		}

	}
	
	
	public static String connRemoteWithJson(String json,String url) throws Exception {
		String result = "";
		CloseableHttpClient httpClient = HttpClients.createDefault();
		try {
			RequestConfig.Builder b = org.apache.http.client.config.RequestConfig.custom();
			b.setSocketTimeout(SOCKET_TIMEOUT);
			b.setConnectTimeout(CONNECTION_TIMEOUT);
			RequestConfig config = b.build();
			
			HttpPost httpPost = new HttpPost(url);
			httpPost.setConfig(config);
			StringEntity entity = new StringEntity(json,ContentType.APPLICATION_JSON);
			
			httpPost.setEntity(entity);
			HttpResponse response = httpClient.execute(httpPost);
			int code = response.getStatusLine().getStatusCode();
			if(code != HttpStatus.SC_OK)
				return result;
			result = EntityUtils.toString(response.getEntity(),"UTF-8");
		} catch (Exception e) {
			System.out.println("can't request: "+e.getMessage());
			throw new Exception(e.getMessage());
		}finally{
			try {
				httpClient.close();
			} catch (Exception e) {
				System.out.println("httpClient close error:"+e.getMessage());
			}
		}
		return result;
	}

	
}
